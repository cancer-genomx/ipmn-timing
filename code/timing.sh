#!/bin/bash
#$ -cwd
#$ -j y
#$ -t 1-901
#$ -l mem_free=2G
#$ -l h_vmem=3G
module load conda_R
rlib="${HOME}/Library/R/3.10-bioc-devel"
Rscript="${HOME}/bin/Rscript"
R_LIBS_USER=$rlib $Rscript "timing.R" $SGE_TASK_ID
